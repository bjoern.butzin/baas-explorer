/* MIT License
 * 
 * Copyright (c) 2017 University of Rostock - Institute of Applied Microelectronics and Computer Engineering
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package eu.itea2.baas.explorer.controller;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapResponse;

import eu.itea2.baas.explorer.BaaSExplorer;

public class DiscoveryClient {
	
	CoapHandler handler;
	
	public DiscoveryClient(){
		this.handler = new CoapHandler() {

			public void onLoad(CoapResponse response) { // also error resp.
				BaaSExplorer.addToConsole("Got response from "+response.advanced().getSource().getHostAddress()+":"+response.advanced().getSourcePort());
				BaaSExplorer.addResponse(response.advanced().getSource().getHostAddress()+":"+response.advanced().getSourcePort(),response.getResponseText());
				System.out.println(response.getResponseText());
			}

			public void onError() { // I/O errors and timeouts
				System.err.println("Failed: I/O error or timeout");
			}
		};
	}

	public void getWellKnown(String serverAddress, int serverPort) {
		CoapClient client1 = new CoapClient(serverAddress+":"+serverPort+"/.well-known/core");
		client1.get(this.handler);

	}

	public void MulticastDiscovery() {
		List<String> addresses = new ArrayList<>();
		addresses.add("224.0.1.187");//IPV4_MC_ADDR
		addresses.add("ff02::fd");//IPV6_LL_MC_ADDR
		addresses.add("ff05::fd");//IPV6_SL_MC_ADDR
		
		for (String address : addresses) {
			CoapClient client = new CoapClient(address+"/.well-known/core");
			client.get(this.handler);
		}
	}
	
	public void queryResourceDirectory(String serverAddress, int serverPort) {
		CoapClient client = new CoapClient(serverAddress+":"+serverPort+"/rd-lookup/res");
		client.get(this.handler);
	}
}
