/* MIT License
 * 
 * Copyright (c) 2017 University of Rostock - Institute of Applied Microelectronics and Computer Engineering
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package eu.itea2.baas.explorer.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class Settings {

	/** the path to the current resource file */
	private static String FILENAME = "BaaSExplorer.properties";

	private static Properties prop;
	private static boolean init = false;

	private static void init() {
		init = true;

		// to ensure fail safe behavior: set default values for each property
		Properties defaultProperties = new Properties();
		for (Property p : Property.values()) {
			defaultProperties.setProperty(p.name(), p.getDefaultvalue());
		}

		// init user properties
		prop = new Properties(defaultProperties);

		// load user properties
		try (FileReader reader = new FileReader(FILENAME);) {
			prop.load(reader);
		} catch (@SuppressWarnings("unused") FileNotFoundException e) {
			// if user property file does not exist; create and set default
			// values as guidance for modification
			File file = new File(FILENAME);
			try {
				file.createNewFile();
				for (Property p : Property.values()) {
					prop.setProperty(p.name(), p.getDefaultvalue());
				}
			} catch (IOException e1) {
				System.err.println(e1.getLocalizedMessage());
			}
		} catch (IOException e) {
			System.err.println(e.getLocalizedMessage());
		}

		// add hook to store changed values on runtime quit
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				store();
			}
		});
	}

	static void store() {
		try (FileWriter out = new FileWriter(FILENAME)) {
			prop.store(out, "BaaS Explorer Properties");
		} catch (IOException e) {
			System.err.println(e.getLocalizedMessage());
		}
	}

	public static String getProperty(Property property) {
		if (!init) {
			init();
		}
		return prop.getProperty(property.name());
	}

	public static void setProperty(Property property, String value) {
		prop.setProperty(property.name(), value);
	}

	public enum Property {
		// Property(default value)
		FONTSIZE("17");

		private String defaultvalue;

		private Property(String defaultValue) {
			this.defaultvalue = defaultValue;
		}

		public String getDefaultvalue() {
			return this.defaultvalue;
		}
	}
}