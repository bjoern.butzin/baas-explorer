/* MIT License
 * 
 * Copyright (c) 2017 University of Rostock - Institute of Applied Microelectronics and Computer Engineering
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package eu.itea2.baas.explorer.view;

import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

public class Autocomplete implements DocumentListener {

	private static enum Mode {
		INSERT, COMPLETION
	}

	JTextField textField;
	private final List<String> keywords;
	Mode mode = Mode.INSERT;

	public Autocomplete(JTextField textField, List<String> keywords) {
		this.textField = textField;
		this.keywords = keywords;
		Collections.sort(keywords);
	}

	@Override
	public void insertUpdate(DocumentEvent ev) {
		if (ev.getLength() != 1)
			return;

		int pos = ev.getOffset();
		String content = null;
		try {
			content = this.textField.getText(0, pos + 1);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}

		// Find where the word starts
		int w;
		for (w = pos; w >= 0; w--) {
			if (!Character.isLetterOrDigit(content.charAt(w)) && !(content.charAt(w) == '_')) {
				break;
			}
		}

		// Too few chars
		if (pos - w < 1) return;

		String prefix = content.substring(w + 1).toLowerCase();

		int n = Collections.binarySearch(this.keywords, prefix);
		if (n < 0 && -n <= this.keywords.size()) {
			String match = this.keywords.get(-n - 1);
			if (match.startsWith(prefix)) {
				// A completion is found
				String completion = match.substring(pos - w);
				// We cannot modify Document from within notification,
				// so we submit a task that does the change later
				SwingUtilities.invokeLater(new CompletionTask(completion, pos + 1));
			}
		} else {
			// Nothing found
			this.mode = Mode.INSERT;
		}
	}

	public class CommitAction extends AbstractAction {
		private static final long serialVersionUID = 5794543109646743416L;

		@Override
		public void actionPerformed(ActionEvent ev) {
			if (Autocomplete.this.mode == Mode.COMPLETION) {
				int pos = Autocomplete.this.textField.getSelectionEnd();
				StringBuffer sb = new StringBuffer(Autocomplete.this.textField.getText());
				Autocomplete.this.textField.setText(sb.toString());
				Autocomplete.this.textField.setCaretPosition(pos);
				Autocomplete.this.mode = Mode.INSERT;
			} else {
				Autocomplete.this.textField.replaceSelection("\t");
			}
		}
	}

	private class CompletionTask implements Runnable {
		private String completion;
		private int position;

		CompletionTask(String completion, int position) {
			this.completion = completion;
			this.position = position;
		}

		public void run() {
			StringBuffer sb = new StringBuffer(Autocomplete.this.textField.getText());
			sb.insert(this.position, this.completion);
			Autocomplete.this.textField.setText(sb.toString());
			Autocomplete.this.textField.setCaretPosition(this.position + this.completion.length());
			Autocomplete.this.textField.moveCaretPosition(this.position);
			Autocomplete.this.mode = Mode.COMPLETION;
		}
	}

	public void changedUpdate(DocumentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void removeUpdate(DocumentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}