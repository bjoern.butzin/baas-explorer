/* MIT License
 * 
 * Copyright (c) 2017 University of Rostock - Institute of Applied Microelectronics and Computer Engineering
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package eu.itea2.baas.explorer.view;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import eu.itea2.baas.explorer.BaaSExplorer;
import eu.itea2.baas.explorer.controller.AnnotatedMutableTreeNode;
import eu.itea2.baas.explorer.controller.DiscoveryClient;
import eu.itea2.baas.explorer.controller.Settings;
import eu.itea2.baas.explorer.controller.Settings.Property;

public class MainView extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JSpinner txtFolding;
	private JTextField txtBafDomain;
	private JTextField txtBafType;
	private JTextField txtBafContext;
	private JTextField txtPort;
	private JTextField txtRD;
	private JTextField txtKvPair;
	private JTextArea txtrConsole;


	private JTree tree;
	private AnnotatedMutableTreeNode root;
	private DefaultTreeModel treeModel;

	private JList<String> detailsList = new JList<>();
	private DefaultListModel<String> listModel = new DefaultListModel<>();

	private int fontsize = Integer.parseInt(Settings.getProperty(Property.FONTSIZE));
	private int fontstyle = Font.BOLD;

	private boolean autoExpand = true;
	private int expandLevel = 1;

	private boolean rdMode;
	
	private int resultCount = 0;
	private JLabel lblResultCountValue;

	public MainView() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/BaaS-Logo.png")));
		setTitle("BaaS Explorer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 768);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(0, 4, 0, 4));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(this.contentPane);

		DocumentListener filterListener = new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				applyFilter();
			}

			public void removeUpdate(DocumentEvent e) {
				applyFilter();
			}

			public void insertUpdate(DocumentEvent e) {
				applyFilter();
			}
		};

		JPanel menu = new JPanel();

		this.contentPane.add(menu, BorderLayout.NORTH);
		menu.setLayout(new BorderLayout(0, 0));

		JPanel buttonPanel = new JPanel();
		menu.add(buttonPanel, BorderLayout.CENTER);
		buttonPanel.setLayout(new BorderLayout(0, 0));

		JPanel discoveryMenu = new JPanel();
		FlowLayout fl_discoveryMenu = (FlowLayout) discoveryMenu.getLayout();
		fl_discoveryMenu.setAlignment(FlowLayout.LEADING);
		buttonPanel.add(discoveryMenu, BorderLayout.WEST);

		JButton multicastDiscoveryBtn = new JButton("MC Discovery");
		multicastDiscoveryBtn.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		discoveryMenu.add(multicastDiscoveryBtn);
		multicastDiscoveryBtn.setHorizontalAlignment(SwingConstants.LEADING);

		JButton rdDiscoveryBtn = new JButton("RD Discovery");
		rdDiscoveryBtn.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		discoveryMenu.add(rdDiscoveryBtn);
		rdDiscoveryBtn.setHorizontalAlignment(SwingConstants.LEADING);

		JLabel lblRdIp = new JLabel("RD Host:");
		lblRdIp.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		discoveryMenu.add(lblRdIp);

		this.txtRD = new JTextField();
		this.txtRD.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		discoveryMenu.add(this.txtRD);
		this.txtRD.setText("localhost");
		this.txtRD.setColumns(16);

		JLabel lblDiscoveryPort = new JLabel("RD Port:");
		lblDiscoveryPort.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		discoveryMenu.add(lblDiscoveryPort);
		lblDiscoveryPort.setLabelFor(this.txtPort);

		this.txtPort = new JTextField();
		this.txtPort.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		discoveryMenu.add(this.txtPort);
		this.txtPort.setText("5683");
		this.txtPort.setColumns(5);
		rdDiscoveryBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				do_rdDiscoveryBtn_actionPerformed(arg0);
			}
		});
		multicastDiscoveryBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				do_multicastDiscoveryBtn_actionPerformed(arg0);
			}
		});

		JPanel iconPanel = new JPanel();
		FlowLayout fl_iconPanel = (FlowLayout) iconPanel.getLayout();
		fl_iconPanel.setAlignment(FlowLayout.TRAILING);
		buttonPanel.add(iconPanel, BorderLayout.EAST);

		JLabel lblIcon = new JLabel("");
		iconPanel.add(lblIcon);
		lblIcon.setIcon(new ImageIcon(getClass().getResource("/BaaS-Logo2.png")));

		JPanel filterPanel = new JPanel();
		menu.add(filterPanel, BorderLayout.SOUTH);
		GridBagLayout gbl_filterPanel = new GridBagLayout();
		gbl_filterPanel.columnWidths = new int[] {0, 0, 0, 0, 0, 0};
		gbl_filterPanel.rowHeights = new int[] {0, 0, 0};
		gbl_filterPanel.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0, 1.0 };
		gbl_filterPanel.rowWeights = new double[] { 1.0, 0.0, 0.0 };
		filterPanel.setLayout(gbl_filterPanel);

		JLabel lblFeature = new JLabel("Folding-Level:");
		lblFeature.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		GridBagConstraints gbc_lblFeature = new GridBagConstraints();
		gbc_lblFeature.anchor = GridBagConstraints.EAST;
		gbc_lblFeature.fill = GridBagConstraints.VERTICAL;
		gbc_lblFeature.insets = new Insets(0, 0, 5, 5);
		gbc_lblFeature.gridx = 0;
		gbc_lblFeature.gridy = 0;
		filterPanel.add(lblFeature, gbc_lblFeature);

		SpinnerNumberModel model1 = new SpinnerNumberModel(1, 1, 9, 1);  
		this.txtFolding = new JSpinner(model1);
		this.txtFolding.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		this.txtFolding.setValue(this.expandLevel);
		lblFeature.setLabelFor(this.txtFolding);
		GridBagConstraints gbc_txtFolding = new GridBagConstraints();
		gbc_txtFolding.insets = new Insets(0, 0, 5, 5);
		gbc_txtFolding.anchor = GridBagConstraints.NORTHWEST;
		gbc_txtFolding.gridx = 1;
		gbc_txtFolding.gridy = 0;
		filterPanel.add(this.txtFolding, gbc_txtFolding);
		this.txtFolding.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				setExpandLevel((int)((JSpinner) arg0.getSource()).getValue());
			}
		});
		
		JLabel lblResults = new JLabel("Results:");
		GridBagConstraints gbc_lblResults = new GridBagConstraints();
		lblResults.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		gbc_lblResults.anchor = GridBagConstraints.WEST;
		gbc_lblResults.insets = new Insets(0, 0, 5, 5);
		gbc_lblResults.gridx = 2;
		gbc_lblResults.gridy = 0;
		filterPanel.add(lblResults, gbc_lblResults);
		
		this.lblResultCountValue = new JLabel();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		this.lblResultCountValue.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 3;
		gbc_textField.gridy = 0;
		filterPanel.add(this.lblResultCountValue, gbc_textField);
		
		JLabel lblBafDomain = new JLabel("BAF Domain:");
		lblBafDomain.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		GridBagConstraints gbc_lblBafDomain = new GridBagConstraints();
		gbc_lblBafDomain.anchor = GridBagConstraints.EAST;
		gbc_lblBafDomain.insets = new Insets(0, 0, 5, 5);
		gbc_lblBafDomain.gridx = 0;
		gbc_lblBafDomain.gridy = 1;
		filterPanel.add(lblBafDomain, gbc_lblBafDomain);

		this.txtBafDomain = new JTextField();
		this.txtBafDomain.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		this.txtBafDomain.setColumns(12);
		this.txtBafDomain.getDocument().addDocumentListener(filterListener);
		lblBafDomain.setLabelFor(this.txtBafDomain);
		GridBagConstraints gbc_txtBafDomain = new GridBagConstraints();
		gbc_txtBafDomain.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtBafDomain.insets = new Insets(0, 0, 5, 5);
		gbc_txtBafDomain.anchor = GridBagConstraints.NORTH;
		gbc_txtBafDomain.gridx = 1;
		gbc_txtBafDomain.gridy = 1;
		filterPanel.add(this.txtBafDomain, gbc_txtBafDomain);

		JLabel lblBafType = new JLabel("BAF Type:");
		lblBafType.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		GridBagConstraints gbc_lblBafType = new GridBagConstraints();
		gbc_lblBafType.anchor = GridBagConstraints.EAST;
		gbc_lblBafType.insets = new Insets(0, 0, 5, 5);
		gbc_lblBafType.gridx = 2;
		gbc_lblBafType.gridy = 1;
		filterPanel.add(lblBafType, gbc_lblBafType);

		this.txtBafType = new JTextField();
		this.txtBafType.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		this.txtBafType.setColumns(12);
		this.txtBafType.getDocument().addDocumentListener(filterListener);
		lblBafType.setLabelFor(this.txtBafType);
		GridBagConstraints gbc_txtBafType = new GridBagConstraints();
		gbc_txtBafType.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtBafType.insets = new Insets(0, 0, 5, 5);
		gbc_txtBafType.anchor = GridBagConstraints.NORTH;
		gbc_txtBafType.gridx = 3;
		gbc_txtBafType.gridy = 1;
		filterPanel.add(this.txtBafType, gbc_txtBafType);

		JLabel lblBafContext = new JLabel("BAF Context:");
		lblBafContext.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		GridBagConstraints gbc_lblBafContext = new GridBagConstraints();
		gbc_lblBafContext.anchor = GridBagConstraints.EAST;
		gbc_lblBafContext.insets = new Insets(0, 0, 5, 5);
		gbc_lblBafContext.gridx = 4;
		gbc_lblBafContext.gridy = 1;
		filterPanel.add(lblBafContext, gbc_lblBafContext);

		this.txtBafContext = new JTextField();
		this.txtBafContext.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		this.txtBafContext.setColumns(12);
		this.txtBafContext.getDocument().addDocumentListener(filterListener);
		lblBafContext.setLabelFor(this.txtBafContext);
		GridBagConstraints gbc_txtBafContext = new GridBagConstraints();
		gbc_txtBafContext.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtBafContext.insets = new Insets(0, 0, 5, 0);
		gbc_txtBafContext.anchor = GridBagConstraints.NORTH;
		gbc_txtBafContext.gridx = 5;
		gbc_txtBafContext.gridy = 1;
		filterPanel.add(this.txtBafContext, gbc_txtBafContext);

		this.txtKvPair = new JTextField();
		this.txtKvPair.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		this.txtKvPair.getDocument().addDocumentListener(filterListener);

		JLabel lblNewLabel = new JLabel("Filter:");
		lblNewLabel.setHorizontalTextPosition(SwingConstants.LEADING);
		lblNewLabel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		lblNewLabel.setToolTipText("Key1=\"Value1 Value 2\" Key2");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 2;
		filterPanel.add(lblNewLabel, gbc_lblNewLabel);
		lblNewLabel.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		lblNewLabel.setLabelFor(this.txtKvPair);
		GridBagConstraints gbc_txtKvpair = new GridBagConstraints();
		gbc_txtKvpair.gridwidth = 5;
		gbc_txtKvpair.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtKvpair.gridx = 1;
		gbc_txtKvpair.gridy = 2;
		filterPanel.add(this.txtKvPair, gbc_txtKvpair);
		this.txtKvPair.setColumns(12);

		// TODO
		// Without this, cursor always leaves text field
		this.txtKvPair.setFocusTraversalKeysEnabled(false);
		// Our words to complete
		ArrayList<String> keywords = new ArrayList<>(5);
		keywords.add("baas_");
		keywords.add("baas_loc__");
		keywords.add("baas_baf__");
		keywords.add("baas_loc__building_id");
		keywords.add("baas_loc__floor_id");
		keywords.add("baas_loc__floor_name");
		keywords.add("baas_loc__room_id");
		keywords.add("baas_baf__type");
		keywords.add("baas_baf__domain");
		keywords.add("baas_baf__context");
		Autocomplete autoComplete = new Autocomplete(this.txtKvPair, keywords);
		this.txtKvPair.getDocument().addDocumentListener(autoComplete);

		// Maps the tab key to the commit action, which finishes the
		// autocomplete when given a suggestion
		final String COMMIT_ACTION = "commit";
		this.txtKvPair.getInputMap().put(KeyStroke.getKeyStroke("TAB"), COMMIT_ACTION);
		this.txtKvPair.getActionMap().put(COMMIT_ACTION, autoComplete.new CommitAction());

		JSplitPane consoleSplitPane = new JSplitPane();
		consoleSplitPane.setResizeWeight(0.8);
		consoleSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		this.contentPane.add(consoleSplitPane, BorderLayout.CENTER);

		JScrollPane scrollPane = new JScrollPane();
		consoleSplitPane.setRightComponent(scrollPane);

		this.txtrConsole = new JTextArea();
		scrollPane.setViewportView(this.txtrConsole);
		this.txtrConsole.setEditable(false);
		this.txtrConsole.setTabSize(4);
		this.txtrConsole.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));

		JSplitPane detailsSplitPane = new JSplitPane();
		detailsSplitPane.setResizeWeight(0.7);
		consoleSplitPane.setLeftComponent(detailsSplitPane);

		JScrollPane treeScrollPane = new JScrollPane();
		detailsSplitPane.setLeftComponent(treeScrollPane);

		this.tree = new JTree();
		this.tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent arg0) {
				do_tree_valueChanged(arg0);
			}
		});
		treeScrollPane.setViewportView(this.tree);
		this.tree.addTreeExpansionListener(new TreeExpansionListener() {
			public void treeCollapsed(TreeExpansionEvent arg0) {
			}

			public void treeExpanded(TreeExpansionEvent arg0) {
				do_tree_treeExpanded(arg0);
			}
		});
		this.tree.setBorder(null);
		this.tree.setRootVisible(false);
		this.tree.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));

		JPanel detailsPanel = new JPanel();
		detailsSplitPane.setRightComponent(detailsPanel);
		detailsPanel.setLayout(new BorderLayout(0, 0));

		JLabel lblDetails = new JLabel("Details:");
		lblDetails.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		detailsPanel.add(lblDetails, BorderLayout.NORTH);

		JScrollPane detailsScrollPane = new JScrollPane();
		detailsPanel.add(detailsScrollPane, BorderLayout.CENTER);
		detailsScrollPane.setViewportView(this.detailsList);
		this.detailsList.setModel(this.listModel);
		this.detailsList.setFont(new Font("DejaVu Sans", this.fontstyle, this.fontsize));
		this.treeModel = (DefaultTreeModel) this.tree.getModel();
		this.root = new AnnotatedMutableTreeNode("");
		this.treeModel.setRoot(this.root);
		this.root.removeAllChildren();
		this.treeModel.reload(this.root);

		setVisible(true);
	}

	public void addToConsole(String line) {
		this.txtrConsole.append(
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z").format(System.currentTimeMillis()) + "\t" + line + "\n");
		this.txtrConsole.setCaretPosition(this.txtrConsole.getDocument().getLength());
	}

	protected void do_rdDiscoveryBtn_actionPerformed(ActionEvent arg0) {
		addToConsole("Querying resource directory @" + this.txtRD.getText() + ":" + this.txtPort.getText());
		BaaSExplorer.clearResponses();
		this.rdMode = true;
		DiscoveryClient discoveryClient = new DiscoveryClient();
		discoveryClient.queryResourceDirectory(this.txtRD.getText(), Integer.parseInt(this.txtPort.getText()));
	}

	protected void do_multicastDiscoveryBtn_actionPerformed(ActionEvent arg0) {
		addToConsole("Running Multicast Discovery");
		BaaSExplorer.clearResponses();
		this.rdMode = false;
		DiscoveryClient discoveryClient = new DiscoveryClient();
		discoveryClient.MulticastDiscovery();
	}

	public void update() {
		Set<Entry<String, String>> responses = BaaSExplorer.getResponses();
		Iterator<Entry<String, String>> it = responses.iterator();

		this.root.removeAllChildren();

		while (it.hasNext()) {
			Entry<String, String> entry = it.next();

			AnnotatedMutableTreeNode node = this.rdMode ? this.root : new AnnotatedMutableTreeNode(entry.getKey());
			if (!this.rdMode) {
				this.root.add(node);
			}

			// process every resource; resources are separated by ',' in core
			// link formats
			String[] resources = entry.getValue().split(",");
			for (String res : resources) {

				// first part is always the path
				String resourcePath = res.split(";")[0].trim();

				// cut off '<', 'coap://' and '>' from path
				resourcePath = resourcePath.substring(1, resourcePath.length() - 1).trim();
				if (resourcePath.toLowerCase().startsWith("coap://")) {
					resourcePath = resourcePath.substring(7).trim();
				}

				// the rest is meta information
				String tags = "";
				if (res.indexOf(';') > 0) {
					tags += res.substring(res.indexOf(';') + 1, res.length());
				}

				// TODO workaround for bug in RD
				resourcePath = resourcePath.replace("//", "/");

				// a leading '/' is removed to avoid empty nodes
				if (resourcePath.startsWith("/")) {
					resourcePath = resourcePath.substring(1);
				}

				// check if nodes along the path already exist and create them
				// if not
				String[] pathSegments = resourcePath.split("/");
				boolean match = true; // initial node matches
				AnnotatedMutableTreeNode last = node;

				for (int i = 0; i < pathSegments.length; i++) {
					AnnotatedMutableTreeNode temp = null;

					// check if node exists
					// we can jump over this if we created the parent node
					// already (!match)
					if (match) {
						match = false;
						for (int j = 0; j < last.getChildCount(); j++) {
							temp = (AnnotatedMutableTreeNode) last.getChildAt(j);
							if (temp.toString().equals(pathSegments[i].trim())) {
								match = true;
								break;
							}
						}
					}
					// if we didn't find a matching node we create one
					if (!match) {
						temp = new AnnotatedMutableTreeNode(pathSegments[i].trim());
						last.add(temp);
					}
					// move to the next path segment
					last = temp;
				}
				// add tags as node annotation
				last.annotate(tags);
			}
		}
	}

	public void applyFilter() {
		update();
		this.resultCount = 0;
		dfs(this.root);
		this.treeModel.reload(this.root);
		if (this.autoExpand) {
			expandTree();
		}
		this.lblResultCountValue.setText(""+this.resultCount);
	}

	private boolean dfs(AnnotatedMutableTreeNode node) {
		boolean result = false;

		if (checkFilter(node.getAnnotation())) {
			return true;
		}
		if (node.isLeaf()) {
			return false;
		}
		AnnotatedMutableTreeNode child = (AnnotatedMutableTreeNode) node.getFirstChild();
		while (null != child) {
			if (dfs(child)) {
				result = true;
				child = (AnnotatedMutableTreeNode) node.getChildAfter(child);
			} else {
				AnnotatedMutableTreeNode rm = child;
				child = (AnnotatedMutableTreeNode) node.getChildAfter(child);
				node.remove(rm);
			}
		}
		return result;
	}

	private boolean checkFilter(String metadata) {
		String[] tags = metadata.split(";");
		Map<String, String> tagMap = new HashMap<>();
		for (String tag : tags) {
			if (!tag.trim().isEmpty() && tag.contains("=")) {
				String key = tag.split("=")[0].trim();
				String value = tag.split("=")[1].trim();
				tagMap.put(key, value);
			}
		}

		Map<String, String> filterMap = new HashMap<>();
		if (!this.txtBafDomain.getText().trim().isEmpty())
			filterMap.put("baas_baf__domain", this.txtBafDomain.getText().trim());
		if (!this.txtBafType.getText().trim().isEmpty())
			filterMap.put("baas_baf__type", this.txtBafType.getText().trim());
		if (!this.txtBafContext.getText().trim().isEmpty())
			filterMap.put("baas_baf__context", this.txtBafContext.getText().trim());

		String[] kvPairs = this.txtKvPair.getText().trim().split(";");
		for (String kvPair : kvPairs) {

			if (!kvPair.trim().isEmpty() && kvPair.contains("=") && kvPair.split("=").length > 1) {
				String key = kvPair.split("=")[0].trim();
				String value = kvPair.split("=")[1].trim();
				filterMap.put(key, value);
			}
		}

		Iterator<String> it = filterMap.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			String value = tagMap.get(key);
			// key not found
			if (null == value) {
				return false;
				// key found
			}
			String[] tagsAvailable = value.split(" ");
			String[] tagsRequired = filterMap.get(key).split(" ");

			// check each tag value that need to be present
			for (String req : tagsRequired) {
				boolean filtermatch = false;
				// compare required tag to all tag values available
				for (String available : tagsAvailable) {
					// found requested value
					if(available.startsWith("\"")){
						available = available.substring(1);
					}
					if (available.toLowerCase().startsWith(req.toLowerCase())) {
						filtermatch = true;
						break;
					}
				}
				// if we gone through all available values without finding
				// the required value
				if (!filtermatch) {
					return false;
				}
			}
		}
		this.resultCount ++;
		if(filterMap.size()==0){
			for(Entry<String, String> e : BaaSExplorer.getResponses()){
				this.resultCount += e.getValue().split(",").length;
			}
		}
		return true;
	}

	public void setExpandLevel(int level){
		this.expandLevel = level;
		expandTree();
	}
	
	public void expandTree() {
		if (this.root.getChildCount() > 0) {
			for (AnnotatedMutableTreeNode node = (AnnotatedMutableTreeNode) this.root
					.getFirstChild(); node != null; node = (AnnotatedMutableTreeNode) node.getNextNode()) {
				if (node.getLevel() < this.expandLevel) {
					this.tree.expandPath(new TreePath(node.getPath()));
				} else if (node.getLevel() == this.expandLevel){
					this.tree.collapsePath(new TreePath(node.getPath()));
				}
			}
		}
	}

	/**
	 * Helper function to expand all children of a node on an expansion event
	 * @param arg0
	 */
	protected void do_tree_treeExpanded(TreeExpansionEvent arg0) {
		AnnotatedMutableTreeNode node = (AnnotatedMutableTreeNode) arg0.getPath().getLastPathComponent();
		if (node.getLevel() > this.expandLevel - 1) {
			AnnotatedMutableTreeNode child;
			for (child = (AnnotatedMutableTreeNode) node
					.getFirstChild(); null != child; child = (AnnotatedMutableTreeNode) child.getNextSibling()) {
				this.tree.expandPath(new TreePath(child.getPath()));
			}
		}
	}

	protected void do_tree_valueChanged(TreeSelectionEvent arg0) {
		AnnotatedMutableTreeNode node = (AnnotatedMutableTreeNode) arg0.getPath().getLastPathComponent();
		String tagString = node.getAnnotation();
		this.listModel.clear();
		for (String kv : tagString.trim().split(";")) {
			this.listModel.addElement(kv);
		}
	}
}
