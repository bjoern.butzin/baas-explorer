/* MIT License
 * 
 * Copyright (c) 2017 University of Rostock - Institute of Applied Microelectronics and Computer Engineering
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package eu.itea2.baas.explorer;

import java.awt.EventQueue;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.UIManager;

import eu.itea2.baas.explorer.view.MainView;

public class BaaSExplorer {

	static MainView view;
	
	private static Map<String,String> responses = new HashMap<>();
	
	public static synchronized void addResponse(String key, String value){
		responses.put(key, value);
		view.applyFilter();
	}
	
	public static void clearResponses(){
		responses.clear();
	}
	
	public static Set<Entry<String,String>> getResponses(){
		return responses.entrySet();
	}
	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					view = new MainView();
				} catch (Exception e) {
					System.err.println(e.getLocalizedMessage());
				}
			}
		});
		
	}
	
	public static void addToConsole(String line){
		view.addToConsole(line);
	}
}
