# Build using Maven

You need to have a working maven installation to build the BaaS Explorer.
Then simply run the following from the project's root directory:

```sh
$ mvn clean install
```

Executable JARs of the examples with all dependencies can be found in the `target\` folder.
 
# Eclipse

The project can be easily imported into a recent version of the Eclipse IDE.
Make sure to have the following before importing the BaaS Explorer project:

* [Eclipse EGit](http://www.eclipse.org/egit/) (should be the case with every recent Eclipse version)
* [m2e - Maven Integration for Eclipse](http://www.eclipse.org/m2e/) (should be the case with every recent Eclipse version)
* UTF-8 workspace text file encoding (Preferences &raquo; General &raquo; Workspace)

Then choose *[Import... &raquo; Maven &raquo; Existing Maven Projects]* to import `BaaSExplorer` into Eclipse.

To run the build process from Eclipse select the Project then right click *[Run As &raquo; Maven install]*

# IntelliJ

The project can also be imported to IntelliJ as follows:

In IntelliJ, choose *[File.. &raquo; Open]* then select the location of the cloned repository in your filesystem.
IntelliJ will then automatically import all projects and resolve required Maven dependencies.

# License
The BaaS Explorer is licensed under MIT License see `license.txt`

# Third Party Software
The BaaS Explorer uses Californium.
Californium is licensed under Eclipse Public License Version 1.0 ("EPL") and Eclipse Distribution License Version 1.0 ("EDL").
See `CaliforniumLicense.txt` for further info.